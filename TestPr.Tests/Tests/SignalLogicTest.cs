using System;
using System.Collections.Generic;
using TestPr.Logics.Impl;
using TestPr.Models;
using Xunit;

namespace TestPr.Tests.Tests
{
    public class SignalLogicTest
    {
        [Fact]
        public void GetWorkTimeFullTimeTest()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(1);
            var to = DateTime.Today + TimeSpan.FromMinutes(7);
            var time = signalLogic.GetWorkTime(data, from, to);
            Assert.Equal(4.0, time.TotalMinutes);
        }
        
        [Fact]
        public void GetWorkTimeFullTimeOverBeforeTest()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(0);
            var to = DateTime.Today + TimeSpan.FromMinutes(7);
            var time = signalLogic.GetWorkTime(data, from, to);
            Assert.Equal(4.0, time.TotalMinutes);
        }
        
        [Fact]
        public void GetWorkTimeFullTimeOverAfterTest()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(1);
            var to = DateTime.Today + TimeSpan.FromMinutes(8);
            var time = signalLogic.GetWorkTime(data, from, to);
            Assert.Equal(5.0, time.TotalMinutes);
        }
        
        [Fact]
        public void GetWorkTimePartTimeTest()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(2);
            var to = DateTime.Today + TimeSpan.FromMinutes(5.5);
            var time = signalLogic.GetWorkTime(data, from, to);
            Assert.Equal(1.5, time.TotalMinutes);
        }
        
        [Fact]
        public void GetWorkTimePartTime2Test()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(2.2);
            var to = DateTime.Today + TimeSpan.FromMinutes(2.8);
            var time = signalLogic.GetWorkTime(data, from, to);
            Assert.Equal(0.6, time.TotalMinutes);
        }
        
        [Fact]
        public void GetWorkTimePartTime3Test()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(1.5);
            var to = DateTime.Today + TimeSpan.FromMinutes(3.5);
            var time = signalLogic.GetWorkTime(data, from, to);
            Assert.Equal(1.5, time.TotalMinutes);
        }
        
        [Fact]
        public void GetDownTimeFullTimeTest()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(1);
            var to = DateTime.Today + TimeSpan.FromMinutes(7);
            var time = signalLogic.GetDownTime(data, from, to);
            Assert.Equal(2.0, time.TotalMinutes);
        }
        
        [Fact]
        public void GetDownTimeFullTimeOverBeforeTest()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(0);
            var to = DateTime.Today + TimeSpan.FromMinutes(7);
            var time = signalLogic.GetDownTime(data, from, to);
            Assert.Equal(2.0, time.TotalMinutes);
        }
        
        [Fact]
        public void GetDownTimeFullTimeOverAfterTest()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(1);
            var to = DateTime.Today + TimeSpan.FromMinutes(8);
            var time = signalLogic.GetDownTime(data, from, to);
            Assert.Equal(2.0, time.TotalMinutes);
        }
        
        [Fact]
        public void GetDownTimePartTimeTest()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(2);
            var to = DateTime.Today + TimeSpan.FromMinutes(4.5);
            var time = signalLogic.GetDownTime(data, from, to);
            Assert.Equal(1.5, time.TotalMinutes);
        }
        
        [Fact]
        public void GetDownTimePartTime2Test()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(3.2);
            var to = DateTime.Today + TimeSpan.FromMinutes(3.8);
            var time = signalLogic.GetDownTime(data, from, to);
            Assert.Equal(0.6, time.TotalMinutes);
        }
        
        [Fact]
        public void GetDownTimePartTime3Test()
        {
            var signalLogic = new SignalLogic();
            var data = GetSortedSignals();
            
            var from = DateTime.Today + TimeSpan.FromMinutes(1.5);
            var to = DateTime.Today + TimeSpan.FromMinutes(3.5);
            var time = signalLogic.GetDownTime(data, from, to);
            Assert.Equal(0.5, time.TotalMinutes);
        }

        private List<EquipmentSignal> GetSortedSignals()
        {
            return new List<EquipmentSignal>
            {
                new EquipmentSignal
                {
                    Time = DateTime.Today + TimeSpan.FromMinutes(1),
                    IsWorking = true,
                },
                new EquipmentSignal
                {
                    Time = DateTime.Today + TimeSpan.FromMinutes(2),
                    IsWorking = true,
                },
                new EquipmentSignal
                {
                    Time = DateTime.Today + TimeSpan.FromMinutes(3),
                    IsWorking = false,
                },
                new EquipmentSignal
                {
                    Time = DateTime.Today + TimeSpan.FromMinutes(4),
                    IsWorking = false,
                },
                new EquipmentSignal
                {
                    Time = DateTime.Today + TimeSpan.FromMinutes(5),
                    IsWorking = true,
                },
                new EquipmentSignal
                {
                    Time = DateTime.Today + TimeSpan.FromMinutes(6),
                    IsWorking = true,
                },
            };
        }
    }
}