using System.Collections.Generic;
using System.Linq;
using TestPr.Models;
using TestPr.Logics.Impl;
using Xunit;
using Record = TestPr.Models.Record;

namespace TestPr.Tests.Tests
{
    public class TreeLogicTest
    {
        [Fact]
        public void RecordsToTreeTest()
        {
            var treeLogic = new TreeLogic();
            var data = GetRecords();
            var treeData = treeLogic.RecordsToTree(data);
            Assert.Single(treeData);
            Assert.Equal(2, treeData.First().Id);
        }
        
        [Fact]
        public void RecordsToReverseTreeTest()
        {
            var treeLogic = new TreeLogic();
            var data = GetRecords();
            var reverseTreeData = treeLogic.RecordsToReverseTree(data);
            Assert.Equal(2, reverseTreeData.Count);
            var rootIds = reverseTreeData.Select(x => x.Id).ToArray();
            Assert.Contains(3, rootIds);
            Assert.Contains(4, rootIds);
        }

        [Fact]
        public void TreeToRecordsTest()
        {
            var treeLogic = new TreeLogic();
            var treeData = GetTreeRecords();
            var data = treeLogic.TreeToRecords(treeData);
            Assert.Equal(5, data.Count);
        }

        [Fact]
        public void CheckCycleBeforeUpdatingTest()
        {
            var treeLogic = new TreeLogic();
            var data = GetRecords();
            var result = treeLogic.CheckCycleBeforeUpdating(data, 2, 1);
            Assert.True(result);
            
            result = treeLogic.CheckCycleBeforeUpdating(data, 2, 4);
            Assert.True(result);
            
            result = treeLogic.CheckCycleBeforeUpdating(data, 4, 3);
            Assert.False(result);
        }

        private List<Record> GetRecords()
        {
            return new List<Record>
            {
                new Record
                {
                    Id = 1,
                    Name = "record 1",
                    ParentId = 2,
                },
                new Record
                {
                    Id = 2,
                    Name = "record 2",
                    ParentId = null,
                },
                new Record
                {
                    Id = 3,
                    Name = "record 3",
                    ParentId = 1,
                },
                new Record
                {
                    Id = 4,
                    Name = "record 4",
                    ParentId = 1,
                },
                new Record
                {
                    Id = 5,
                    Name = "record 5",
                    ParentId = null
                }
            };
        }

        private List<RecordTree> GetTreeRecords()
        {
            return new List<RecordTree>
            {
                new RecordTree
                {
                    Id = 1,
                    Name = "Record 1",
                    Children = new List<RecordTree>
                    {
                        new RecordTree
                        {
                            Id = 2,
                            Name = "Record 1.1",
                            ParentId = 1,
                        },
                        new RecordTree
                        {
                            Id = 3,
                            Name = "Record 1.2",
                            ParentId = 1,
                            Children = new List<RecordTree>
                            {
                                new RecordTree
                                {
                                    Id = 4,
                                    Name = "Record 1.2.1",
                                    ParentId = 3,
                                }
                            }
                        },
                    },
                },
                new RecordTree
                {
                    Id = 5,
                    Name = "Record 2",
                }
            };
        }
    }
}