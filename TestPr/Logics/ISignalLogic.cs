using System;
using System.Collections.Generic;
using TestPr.Models;

namespace TestPr.Logics
{
    public interface ISignalLogic
    {
        /// <summary>
        /// Вычисление времени работы на основе списка сигналов оборудования.
        /// </summary>
        /// <param name="signals">Список сигналов с оборудования.</param>
        /// <param name="from">Время начала вычесляемого периода.</param>
        /// <param name="to">Время завершения вычесляемого периода.</param>
        /// <returns>Суммарное время работы оборудования за период.</returns>
        TimeSpan GetWorkTime(List<EquipmentSignal> signals, DateTime from, DateTime to);
        
        /// <summary>
        /// Вычисление времени простоя на основе списка сигналов оборудования.
        /// </summary>
        /// <param name="signals">Список сигналов с оборудования.</param>
        /// <param name="from">Время начала вычесляемого периода.</param>
        /// <param name="to">Время завершения вычесляемого периода.</param>
        /// <returns>Суммарное время простоя оборудования за период.</returns>
       TimeSpan GetDownTime(List<EquipmentSignal> signals, DateTime from, DateTime to);
    }
}