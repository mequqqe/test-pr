using System;
using System.Collections.Generic;
using TestPr.Models;

namespace TestPr.Logics.Impl
{
    public class SignalLogic: ISignalLogic
    {
        public TimeSpan GetWorkTime(List<EquipmentSignal> signals, DateTime from, DateTime to)
        {
            TimeSpan workTime = TimeSpan.Zero;
            foreach (var signal in signals)
            {
                if (signal.Time >= from && signal.Time <= to && signal.IsWorking)
                {
                    workTime += TimeSpan.FromMinutes(1);
                }
            }
            return workTime;
        }

        public TimeSpan GetDownTime(List<EquipmentSignal> signals, DateTime from, DateTime to)
        {
            TimeSpan downTime = TimeSpan.Zero;
            bool isEquipmentDown = false;

            foreach (var signal in signals)
            {
                if (signal.Time >= from && signal.Time <= to)
                {
                    if (!signal.IsWorking && !isEquipmentDown)
                    {
                        
                        isEquipmentDown = true;
                    }
                    else if (signal.IsWorking && isEquipmentDown)
                    {
                       
                        downTime += signal.Time - from;
                        isEquipmentDown = false;
                    }
                }
            }
            if (isEquipmentDown)
            {
                downTime += to - from;
            }

            return downTime;
        }

    }
}