using System;
using System.Collections.Generic;
using System.Linq;
using TestPr.Models;



namespace TestPr.Logics.Impl
{
    public class TreeLogic : ITreeLogic
    {
        public List<RecordTree> RecordsToTree(List<Record> records)
        {
            var rootNodes = new List<RecordTree>();
            var nodeLookup = new Dictionary<int, RecordTree>();

            foreach (var record in records)
            {
                var treeNode = new RecordTree
                {
                    Id = record.Id,
                    Name = record.Name,
                    Children = new List<RecordTree>()
                };
                nodeLookup[record.Id] = treeNode;

                if (record.ParentId == null)
                {
                    rootNodes.Add(treeNode);
                }
                else
                {
                    if (nodeLookup.ContainsKey(record.ParentId.Value))
                    {
                        nodeLookup[record.ParentId.Value].Children.Add(treeNode);
                    }
                }
            }

            return rootNodes;
        }

        public List<Record> TreeToRecords(List<RecordTree> records)
        {
            var result = new List<Record>();
            FlattenTree(records, result);
            return result;
        }

        private void FlattenTree(IEnumerable<RecordTree> nodes, List<Record> result, int? parentId = null)
        {
            foreach (var node in nodes)
            {
                var record = new Record
                {
                    Id = node.Id,
                    Name = node.Name,
                    ParentId = parentId
                };
                result.Add(record);
                FlattenTree(node.Children, result, node.Id);
            }
        }

        public List<RecordReverseTree> RecordsToReverseTree(List<Record> records)
        {
            var rootNodes = new List<RecordReverseTree>();
            var nodeLookup = new Dictionary<int, RecordReverseTree>();

            foreach (var record in records)
            {
                var treeNode = new RecordReverseTree
                {
                    Id = record.Id,
                    Name = record.Name,
                    Parents = new List<RecordReverseTree>()
                };
                nodeLookup[record.Id] = treeNode;

                if (record.ParentId == null)
                {
                    rootNodes.Add(treeNode);
                }
                else
                {
                    if (nodeLookup.ContainsKey(record.ParentId.Value))
                    {
                        nodeLookup[record.ParentId.Value].Parents.Add(treeNode);
                    }
                }
            }

            return rootNodes;
        }

        public bool CheckCycleBeforeUpdating(List<Record> records, int id, int newParentId)
        {
            var visitedNodes = new HashSet<int>();
            visitedNodes.Add(id);
            return HasCycle(records, id, newParentId, visitedNodes);
        }

        private bool HasCycle(List<Record> records, int currentId, int targetParentId, HashSet<int> visitedNodes)
        {
            foreach (var record in records.Where(r => r.ParentId == currentId))
            {
                if (record.Id == targetParentId || visitedNodes.Contains(record.Id))
                {
                    return true; 
                }

                visitedNodes.Add(record.Id);

                if (HasCycle(records, record.Id, targetParentId, visitedNodes))
                {
                    return true;
                }

                visitedNodes.Remove(record.Id);
            }

            return false; 
        }
    }
}
