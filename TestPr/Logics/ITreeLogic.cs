using System.Collections.Generic;
using TestPr.Models;

namespace TestPr.Logics
{
    public interface ITreeLogic
    {
        /// <summary>
        /// Получение дерева объектов из списка записей
        /// </summary>
        /// <param name="records"></param>
        /// <returns></returns>
        List<RecordTree> RecordsToTree(List<Record> records);
        
        /// <summary>
        /// Получение списка записей из дерева объектов
        /// </summary>
        /// <param name="records"></param>
        /// <returns></returns>
        List<Record> TreeToRecords(List<RecordTree> records);

        /// <summary>
        /// Получение обратного дерева объектов из списка
        /// </summary>
        /// <param name="records"></param>
        /// <returns></returns>
        List<RecordReverseTree> RecordsToReverseTree(List<Record> records);

        /// <summary>
        /// Проверка на зацикливание при обновлении записи
        /// </summary>
        /// <param name="records"></param>
        /// <param name="id"></param>
        /// <param name="newParentId"></param>
        /// <returns>false - зацикливания нет, true - есть зацикливание</returns>
        bool CheckCycleBeforeUpdating(List<Record> records, int id, int newParentId);
    }
}