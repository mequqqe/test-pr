﻿// Интерфейс сервисного слоя
using System.Collections.Generic;
using System;
using TestPr.Logics;
using TestPr.Models;

public interface ISignalService
{
    TimeSpan GetWorkTime(List<EquipmentSignal> signals, DateTime from, DateTime to);
    TimeSpan GetDownTime(List<EquipmentSignal> signals, DateTime from, DateTime to);
}

// Реализация сервисного слоя
public class SignalService : ISignalService
{
    private readonly ISignalLogic signalLogic;

    public SignalService(ISignalLogic signalLogic)
    {
        this.signalLogic = signalLogic;
    }

    public TimeSpan GetWorkTime(List<EquipmentSignal> signals, DateTime from, DateTime to)
    {
        return signalLogic.GetWorkTime(signals, from, to);
    }

    public TimeSpan GetDownTime(List<EquipmentSignal> signals, DateTime from, DateTime to)
    {
        return signalLogic.GetDownTime(signals, from, to);
    }
}

