﻿using System;
using System.Collections.Generic;
using TestPr.Models;
using TestPr.Logics;

public interface ITreeLogicService
{
    List<RecordTree> RecordsToTree(List<Record> records);
    List<Record> TreeToRecords(List<RecordTree> records);
    List<RecordReverseTree> RecordsToReverseTree(List<Record> records);
    bool CheckCycleBeforeUpdating(List<Record> records, int id, int newParentId);
}

public class TreeLogicService : ITreeLogicService
{
    private readonly ITreeLogic treeLogic;

    public TreeLogicService(ITreeLogic treeLogic)
    {
        this.treeLogic = treeLogic;
    }

    public List<RecordTree> RecordsToTree(List<Record> records)
    {
        return treeLogic.RecordsToTree(records);
    }

    public List<Record> TreeToRecords(List<RecordTree> records)
    {
        return treeLogic.TreeToRecords(records);
    }

    public List<RecordReverseTree> RecordsToReverseTree(List<Record> records)
    {
        return treeLogic.RecordsToReverseTree(records);
    }

    public bool CheckCycleBeforeUpdating(List<Record> records, int id, int newParentId)
    {
        return treeLogic.CheckCycleBeforeUpdating(records, id, newParentId);
    }
}
