﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using TestPr.Models;

[Route("api/equipment")]
[ApiController]
public class EquipmentController : ControllerBase
{
    private readonly ISignalService signalService;

    public EquipmentController(ISignalService signalService)
    {
        this.signalService = signalService;
    }

    [HttpGet("work-time")]
    public IActionResult GetWorkTime([FromBody] List<EquipmentSignal> signals, DateTime from, DateTime to)
    {
        TimeSpan workTime = signalService.GetWorkTime(signals, from, to);
        return Ok(workTime);
    }

    [HttpGet("down-time")]
    public IActionResult GetDownTime([FromBody] List<EquipmentSignal> signals, DateTime from, DateTime to)
    {
        TimeSpan downTime = signalService.GetDownTime(signals, from, to);
        return Ok(downTime);
    }
}

