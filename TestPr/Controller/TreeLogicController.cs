﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TestPr.Models;

[Route("api/tree")]
[ApiController]
public class TreeLogicController : ControllerBase
{
    private readonly ITreeLogicService treeLogicService;

    public TreeLogicController(ITreeLogicService treeLogicService)
    {
        this.treeLogicService = treeLogicService;
    }

    [HttpPost("records-to-tree")]
    public IActionResult RecordsToTree([FromBody] List<Record> records)
    {
        var tree = treeLogicService.RecordsToTree(records);
        return Ok(tree);
    }

    [HttpPost("tree-to-records")]
    public IActionResult TreeToRecords([FromBody] List<RecordTree> tree)
    {
        var records = treeLogicService.TreeToRecords(tree);
        return Ok(records);
    }

    [HttpPost("records-to-reverse-tree")]
    public IActionResult RecordsToReverseTree([FromBody] List<Record> records)
    {
        var reverseTree = treeLogicService.RecordsToReverseTree(records);
        return Ok(reverseTree);
    }

    [HttpGet("check-cycle-before-updating")]
    public IActionResult CheckCycleBeforeUpdating(int id, int newParentId, [FromBody] List<Record> records)
    {
        bool hasCycle = treeLogicService.CheckCycleBeforeUpdating(records, id, newParentId);
        return Ok(hasCycle);
    }
}
