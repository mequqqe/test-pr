using System.Collections.Generic;

namespace TestPr.Models
{
    public class RecordTree
    {
        public int Id { get; set; }
        
        public int? ParentId { get; set; }
        
        public string Name { get; set; }
        
        public List<RecordTree> Children { get; set; }
    }
}