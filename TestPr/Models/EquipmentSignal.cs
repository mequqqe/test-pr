using System;

namespace TestPr.Models
{
    /// <summary>
    /// Модель сигнала (в работе/простой) оборудования.
    /// </summary>
    public class EquipmentSignal
    {
        /// <summary>
        /// Время показания сигнала.
        /// </summary>
        public DateTime Time { get; set; }
        
        /// <summary>
        /// Значение сигнала (true = в работе, false = простой).
        /// </summary>
        public bool IsWorking { get; set; }
    }
}