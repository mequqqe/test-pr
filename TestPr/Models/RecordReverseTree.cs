using System.Collections.Generic;

namespace TestPr.Models
{
    public class RecordReverseTree
    {
        public int Id { get; set; }
        
        public int? ParentId { get; set; }
        
        public string Name { get; set; }
        
        public RecordReverseTree Parent { get; set; }
        public List<RecordReverseTree> Parents { get; internal set; }
    }
}